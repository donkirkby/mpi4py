==============
MPI for Python
==============

Overview
--------

Welcome to MPI for Python. This package provides Python bindings for
the *Message Passing Interface* (`MPI <http://www.mpi-forum.org/>`_)
standard. It is implemented on top of the MPI-1/2/3 specification and
exposes an API which grounds on the standard MPI-2 C++ bindings.

Dependencies
------------

* `Python <http://www.python.org/>`_ 2.4 to 2.7 or 3.0 to 3.4, or a
  recent `PyPy <http://pypy.org/>`_ release.

* A functional MPI 1.x/2.x/3.x implementation like `MPICH
  <http://www.mpich.org/>`_ or `Open MPI <http://www.open-mpi.org/>`_
  built with shared/dynamic libraries.

* To work with the in-development version, you need to install `Cython
  <http://www.cython.org/>`_.
